import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:flutter_map_booking/Screen/SplashScreen/SplashScreen.dart';
import 'package:flutter_map_booking/Screen/Home/home3.dart';
import 'package:flutter_map_booking/Screen/Login/login.dart';
import 'package:flutter_map_booking/Screen/SignUp/signup.dart';
import 'package:flutter_map_booking/Screen/SignUp/signup2.dart';
import 'package:flutter_map_booking/Screen/Directions/directions.dart';
import 'package:flutter_map_booking/Screen/ReviewTrip/reviewTrip.dart';
import 'package:flutter_map_booking/Screen/Walkthrough/walkthrough.dart';
import 'package:flutter_map_booking/Screen/Walkthrough/walkthrough2.dart';
import 'package:flutter_map_booking/Screen/Login/login2.dart';
import 'package:flutter_map_booking/Screen/History/history.dart';
import 'package:flutter_map_booking/Screen/Home/home.dart';
import 'package:flutter_map_booking/Screen/Home/selectAddress.dart';

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    if (animation.status == AnimationStatus.reverse)
      return super.buildTransitions(context, animation, secondaryAnimation, child);
    return FadeTransition(opacity: animation, child: child);
  }
}

class Routes {
  Routes() {
    runApp(new MaterialApp(
      title: "Hey taksi",
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/': return new MyCustomRoute(
            builder: (_) => new LoginScreen(),
            settings: settings,
          );
          case '/login': return new MyCustomRoute(
            builder: (_) => new LoginScreen(),
            settings: settings,
          );
          case '/login2': return new MyCustomRoute(
            builder: (_) => LoginScreen2(),
            settings: settings,
          );
          case '/signup': return new MyCustomRoute(
            builder: (_) => new SignUpScreen(),
            settings: settings,
          );
          case '/signup2': return new MyCustomRoute(
            builder: (_) => new SignupScreen2(),
            settings: settings,
          );
          case '/home': return new MyCustomRoute(
            builder: (_) => new HomeScreen(),
            settings: settings,
          );
          case '/home2': return new MyCustomRoute(
            builder: (_) => new HomeScreen2(),
            settings: settings,
          );
          case '/forgot_password': return new MyCustomRoute(
            builder: (_) => new HomeScreen2(),
            settings: settings,
          );
          case '/directions': return new MyCustomRoute(
            builder: (_) => new DirectionsScreen(),
            settings: settings,
          );
          case '/review_trip': return new MyCustomRoute(
            builder: (_) => ReviewTripScreens(),
            settings: settings,
          );
          case '/walkthrough': return new MyCustomRoute(
            builder: (_) => WalkthroughScreens(),
            settings: settings,
          );
          case '/walkthrough2': return new MyCustomRoute(
            builder: (_) => WalkthroughScreen2(),
            settings: settings,
          );
          case '/history': return new MyCustomRoute(
            builder: (_) => HistoryScreen(),
            settings: settings,
          );
        }
        assert(false);
      },
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
      theme: appTheme,
      //routes: routes,
    ));
  }
}