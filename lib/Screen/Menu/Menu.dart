import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_map_booking/Screen/Login/login2.dart';
import 'package:flutter_map_booking/Screen/PaymentMethod/paymentMethod.dart';
import 'package:flutter_map_booking/Screen/Notification/notification.dart';
import 'package:flutter_map_booking/Screen/History/history.dart';
import 'package:flutter_map_booking/Screen/Settings/settings.dart';
import 'package:flutter_map_booking/Screen/MyProfile/myProfile.dart';

class MenuItems {
  String name;
  IconData icon;
  MenuItems({this.icon, this.name});
}

class MenuScreens extends StatelessWidget {
  final String activeScreenName;

  MenuScreens({this.activeScreenName});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          UserAccountsDrawerHeader(
            margin: EdgeInsets.all(0.0),
            accountName: new Text("John",style: headingWhite,),
            accountEmail: new Text("100 point - Gold member"),
            currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Image(
                    width: 100.0,
                    image: new AssetImage('assets/image/taxi-driver.png',)
                )
            ),
            onDetailsPressed: (){
              Navigator.pop(context);
              Navigator.of(context).push(new MaterialPageRoute<Null>(
                  builder: (BuildContext context) {
                    return MyProfile();
                  },
                  fullscreenDialog: true));
            },
          ),
          new MediaQuery.removePadding(
            context: context,
            // DrawerHeader consumes top MediaQuery padding.
            removeTop: true,
            child: new Expanded(
              child: new ListView(
                //padding: const EdgeInsets.only(top: 8.0),
                children: <Widget>[
                  new Stack(
                    children: <Widget>[
                      // The initial contents of the drawer.
                      new Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);},
                            child: new Container(
                              height: 60.0,
                              color: this.activeScreenName.compareTo("HOME") == 0 ? greyColor : whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.home,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('Home',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushNamedAndRemoveUntil('/home2', (Route<dynamic> route) => false);},
                            child: new Container(
                              height: 60.0,
                              color: this.activeScreenName.compareTo("HOME2") == 0 ? greyColor : whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.home,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('Home 2',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushReplacement(
                                  new MaterialPageRoute(builder: (context) => PaymentMethod()));
                            },
                            child: new Container(
                              height: 60.0,
                              color: this.activeScreenName.compareTo("PAYMENT") == 0 ? greyColor : whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.wallet,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('Payment',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushReplacement(
                                  new MaterialPageRoute(builder: (context) => HistoryScreen()));
                            },
                            child: new Container(
                              height: 60.0,
                              color: this.activeScreenName.compareTo("HISTORY") == 0 ? greyColor : whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.history,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('History',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushReplacement(
                                  new MaterialPageRoute(builder: (context) => NotificationScreens()));
                            },
                            child: new Container(
                              height: 60.0,
                              color: this.activeScreenName.compareTo("NOTIFICATIONS") == 0 ? greyColor : whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.bell,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('Notifications',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushReplacement(
                                  new MaterialPageRoute(builder: (context) => SettingsScreen()));
                              },
                            child: new Container(
                              height: 60.0,
                              color: this.activeScreenName.compareTo("SETTINGS") == 0 ? greyColor : whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.cogs,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('Settings',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).pushReplacement(
                                  new MaterialPageRoute(builder: (context) => LoginScreen2()));
                            },
                            child: new Container(
                              height: 60.0,
                              color: whiteColor,
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 1,
                                    child: Icon(FontAwesomeIcons.signOutAlt,color: blackColor,),
                                  ),
                                  new Expanded(
                                    flex: 3,
                                    child: new Text('Logout',style: headingBlack,),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      // The drawer's "details" view.
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
