import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter/services.dart';
import 'package:flutter_map_booking/Components/loading.dart';
import 'package:flutter_map_booking/Components/historyTrip.dart';
import 'package:flutter_map_booking/Screen/Menu/Menu.dart';
import 'package:flutter_map_booking/Screen/SearchAddress/searchAddress2.dart';
import 'package:flutter_map_booking/Model/placeItem.dart';
import 'dart:io' show Platform;
import 'dart:math' show cos, sqrt, asin;
import 'package:flutter/services.dart' show rootBundle;
import 'radioSelectMapType.dart';
import 'package:flutter_map_booking/Model/mapTypeModel.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final String screenName = "HOME";
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{};

  CircleId selectedCircle;
  int _markerIdCounter = 0;
  GoogleMapController _mapController;
  BitmapDescriptor _markerIcon;

  String currentLocationName;
  String newLocationName;
  Position _currentPosition;
  String _placemark = '';
  GoogleMapController mapController;
  CameraPosition _position;
  PlaceItemRes fromAddress;
  PlaceItemRes toAddress;
  bool checkPlatform = Platform.isIOS;
  double distance = 0;
  bool _nightMode = false;
  VoidCallback _showPersBottomSheetCallBack;
  List<MapTypeModel> sampleData = new List<MapTypeModel>();
  PersistentBottomSheetController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCurrentLocation();
    _showPersBottomSheetCallBack = _showBottomSheet;
    sampleData.add(MapTypeModel(1,true, 'assets/style/maptype_nomal.png', 'Nomal', 'assets/style/nomal_mode.json'));
    sampleData.add(MapTypeModel(2,false, 'assets/style/maptype_silver.png', 'Silver', 'assets/style/sliver_mode.json'));
    sampleData.add(MapTypeModel(3,false, 'assets/style/maptype_dark.png', 'Dark', 'assets/style/dark_mode.json'));
    sampleData.add(MapTypeModel(4,false, 'assets/style/maptype_night.png', 'Night', 'assets/style/night_mode.json'));
    sampleData.add(MapTypeModel(5,false, 'assets/style/maptype_netro.png', 'Netro', 'assets/style/netro_mode.json'));
    sampleData.add(MapTypeModel(6,false, 'assets/style/maptype_aubergine.png', 'Aubergine', 'assets/style/aubergine_mode.json'));
  }

  @override
  void dispose() {
    super.dispose();
  }

  /// Get current location
  Future<void> _getCurrentLocation() async {
    Position position;
    try {
      final Geolocator geolocator = Geolocator()
        ..forceAndroidLocationManager = true;
      position = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.bestForNavigation);
    } on PlatformException {
      position = null;
    }
    if (!mounted) {
      return;
    }
    setState(() {
      _currentPosition = position;
      print(_currentPosition.longitude);
      print(_currentPosition.latitude);
    });
    List<Placemark> placemarks = await Geolocator().placemarkFromCoordinates(_currentPosition.latitude, _currentPosition.longitude);
    if (placemarks != null && placemarks.isNotEmpty) {
      final Placemark pos = placemarks[0];
      setState(() {
        _placemark = pos.name + ', ' + pos.thoroughfare;
        print(_placemark);
        currentLocationName = _placemark;
      });
    }
  }

  /// Get current location name
  void getLocationName(double lat, double lng) async {
    List<Placemark> placemarks = await Geolocator().placemarkFromCoordinates(lat, lng);
    if (placemarks != null && placemarks.isNotEmpty) {
      final Placemark pos = placemarks[0];
      setState(() {
        _placemark = pos.name + ', ' + pos.thoroughfare;
        newLocationName = _placemark;
      });
    }
  }

  void _onMapCreated(GoogleMapController controller) async {
    this._mapController = controller;
    MarkerId markerId = MarkerId(_markerIdVal());
    LatLng position = LatLng(_currentPosition.latitude != null ? _currentPosition.latitude : 0.0, _currentPosition.longitude != null ? _currentPosition.longitude : 0.0);
    Marker marker = Marker(
      markerId: markerId,
      position: position,
      draggable: false,
    );
    setState(() {
      _markers[markerId] = marker;
    });
    Future.delayed(Duration(milliseconds: 200), () async {
      this._mapController = controller;
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: position,
            zoom: 15.0,
          ),
        ),
      );
    });
  }

  String _markerIdVal({bool increment = false}) {
    String val = 'marker_id_$_markerIdCounter';
    if (increment) _markerIdCounter++;
    return val;
  }

  submitLocation(){
    print(_position);
    print(newLocationName);
  }

  Future<String> _getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  void _setMapStyle(String mapStyle) {
    setState(() {
      _nightMode = true;
      _mapController.setMapStyle(mapStyle);
    });
  }

  void changeMapType(int id, String fileName){
    print(fileName);
    if (fileName == null) {
      setState(() {
        _nightMode = false;
        _mapController.setMapStyle(null);
      });
    } else {
      _getFileData(fileName).then(_setMapStyle);
    }
  }

  void _showBottomSheet() async {
    setState(() {
      _showPersBottomSheetCallBack = null;
    });
    _controller = await _scaffoldKey.currentState
        .showBottomSheet((context) {
      return new Container(
        height: 300.0,
        child: Container(
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text("Map type",style: heading18Black,),
                  ),
                  Container(
                    child: IconButton(
                      icon: Icon(Icons.close,color: blackColor,),
                      onPressed: (){
                        Navigator.pop(context);
                      },
                    ),
                  )
                ],
              ),
              Expanded(
                child:
                new GridView.builder(
                  itemCount: sampleData.length,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                  itemBuilder: (BuildContext context, int index) {
                    return new InkWell(
                      highlightColor: Colors.red,
                      splashColor: Colors.blueAccent,
                      onTap: () {
                        _closeModalBottomSheet();
                        sampleData.forEach((element) => element.isSelected = false);
                        sampleData[index].isSelected = true;
                        changeMapType(sampleData[index].id, sampleData[index].fileName);

                      },
                      child: new MapTypeItem(sampleData[index]),
                    );
                  },
                ),
              )

            ],
          ),
        )
      );
    });
  }

  void _closeModalBottomSheet() {
    if (_controller != null) {
      _controller.close();
      _controller = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        drawer: new MenuScreens(activeScreenName: screenName),
        body: _currentPosition == null ? Container(
          child: LoadingBuilder(),
        ): SingleChildScrollView(
          child: Container(
            color: whiteColor,
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    SizedBox(
                      height: MediaQuery.of(context).size.height - 110,
                      child: GoogleMap(
                        markers: Set<Marker>.of(_markers.values),

                        onMapCreated: _onMapCreated,
                        myLocationEnabled: true,
                        myLocationButtonEnabled: false,
                        initialCameraPosition: CameraPosition(
                          target: LatLng(_currentPosition.latitude != null ? _currentPosition.latitude : 0.0, _currentPosition.longitude != null ? _currentPosition.longitude : 0.0),
                          zoom: 12.0,
                        ),
                        onCameraMove: (CameraPosition position) {
                          if(_markers.length > 0) {
                            MarkerId markerId = MarkerId(_markerIdVal());
                            Marker marker = _markers[markerId];
                            Marker updatedMarker = marker.copyWith(
                              positionParam: position.target,
                            );
                            setState(() {
                              _markers[markerId] = updatedMarker;
                              _position = position;
                            });
                          }
                        },
                        onCameraIdle: () => getLocationName(
                            _position.target.latitude != null ? _position.target.latitude : _currentPosition.latitude,
                            _position.target.longitude != null ? _position.target.longitude : _currentPosition.longitude
                        ),
                      ),
                    ),
                    Container(
                      //color: greyColor,
                        height: 110.0,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                                onTap: (){
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SearchAddress2(
                                        newLocationName != null ? newLocationName : "",
                                        _position,
                                      ),
                                      fullscreenDialog: true
                                  ));
                                },
                                child: HistoryTrip(
                                  fromAddress: newLocationName != null ? newLocationName : "",
                                  toAddress: "To address",
                                )
                            )
                          ],
                        )

                    ),
                  ],
                ),
                Positioned(
                  bottom: 120,
                  right: 16,
                  child: Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(100.0),),
                    ),
                    child: IconButton(
                      icon: Icon(Icons.my_location,size: 20.0,color: blackColor,),
                      onPressed: (){
                        _mapController.animateCamera(
                          CameraUpdate.newCameraPosition(
                            CameraPosition(
                              target: LatLng(_currentPosition.latitude != null ? _currentPosition.latitude : 0.0, _currentPosition.longitude != null ? _currentPosition.longitude : 0.0),
                              zoom: 17.0,
                            ),
                          ),
                        );
                      },
                    ),
                  )
                ),
                Positioned(
                  top: 60,
                  right: 10,
                  child: Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(100.0),),
                    ),
                    child: IconButton(
                      icon: Icon(Icons.layers,size: 20.0,color: blackColor,),
                      onPressed: (){
                        _showBottomSheet();
                      },
                    ),
                  )
                ),
                Positioned(
                    top: 60,
                    left: 10,
                    child: Container(
                      height: 40.0,
                      width: 40.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(100.0),),
                      ),
                      child: IconButton(
                        icon: Icon(Icons.menu,size: 20.0,color: blackColor,),
                        onPressed: (){
                          _scaffoldKey.currentState.openDrawer();
                        },
                      ),
                    )
                ),
              ],
            ),
          )
        ),
    );
  }
}
