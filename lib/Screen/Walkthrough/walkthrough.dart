import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart' as prefix0;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:flutter_map_booking/Screen/Home/home3.dart';

class WalkthroughScreens extends StatelessWidget {
  final pages = [
    PageViewModel(
        pageColor: const Color(0xFFFFD428),
        // iconImageAssetPath: 'assets/air-hostess.png',
        //bubble: Image.asset('assets/image/touchscreen-100.png'),
        body: Text(
          'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
          style: prefix0.headingWhite,
        ),
        title: Text(
          'Accept a JOB',
          style: prefix0.heading35,
        ),
        textStyle: TextStyle(fontFamily: 'OpenSans', color: Colors.white),
        mainImage: Image.asset(
          'assets/image/1.png',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFFFFD428),
//      iconImageAssetPath: 'assets/waiter.png',
      body: Text(
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        style: prefix0.headingWhite,
      ),
      title: Text('Tracking Realtime',style: prefix0.heading35,),
      mainImage: Image.asset(
        'assets/image/2.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(fontFamily: 'OpenSans', color: Colors.white),
    ),
    PageViewModel(
      pageColor: const Color(0xFFFFD428),
//      iconImageAssetPath: 'assets/taxi-driver.png',
      body: Text(
        'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
        style: prefix0.headingWhite,
      ),
      title: Text('Earn Money',style: prefix0.heading35,),
      mainImage: Image.asset(
        'assets/image/3.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(fontFamily: 'OpenSans', color: Colors.white),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IntroViewsFlutter(
        pages,
        showNextButton:true,
        onTapDoneButton: () {
          Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
        },
        pageButtonTextStyles: TextStyle(
          color: Colors.white,
          fontSize: 18.0,
        ),
      ),
    );
  }
}

