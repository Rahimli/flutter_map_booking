import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:flutter_map_booking/Screen/Menu/Menu.dart';
import 'package:flutter_map_booking/Components/historyTrip.dart';
import 'detail.dart';

class HistoryScreen extends StatefulWidget {
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  final String screenName = "HISTORY";

  navigateToDetail(String id) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => HistoryDetail(id: id,)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'History',
          style: TextStyle(color: blackColor),
        ),
        backgroundColor: whiteColor,
        elevation: 2.0,
        iconTheme: IconThemeData(color: blackColor),
      ),
      drawer: new MenuScreens(activeScreenName: screenName),
      body: new ListView.builder(
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return Container(
                color: greyColor,
                child: GestureDetector(
                    onTap: () {
                      print('$index');
                      navigateToDetail(index.toString());
                    },
                    child: historyItem()));
          }),
    );
  }

  Widget historyItem() {
    return Container(
      padding: EdgeInsets.all(10.0),
      margin: EdgeInsets.all(10.0),
      color: whiteColor,
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text("\$20.5", style: textBoldBlack,),
                ),
                Container(
                  child: Text("08 Jan 2019 at 12:00 PM", style: textBoldBlack,),
                ),
              ],
            ),
          ),
          Container(
            child: HistoryTrip(
              fromAddress: "994 Colin Gateway Suite, 981",
              toAddress: "8753 Mauricio Walks",
            ),
          ),
        ],
      ),
    );
  }
}
