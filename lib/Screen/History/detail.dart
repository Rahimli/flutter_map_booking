import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_map_booking/Components/historyTrip.dart';
import 'history.dart';

class HistoryDetail extends StatefulWidget {
  final String id;

  HistoryDetail({this.id});

  @override
  _HistoryDetailState createState() => _HistoryDetailState();
}

class _HistoryDetailState extends State<HistoryDetail> {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  String yourReview;
  double ratingScore;

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'History',
          style: TextStyle(color: blackColor),
        ),
        backgroundColor: whiteColor,
        elevation: 2.0,
        iconTheme: IconThemeData(color: blackColor),
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
          child: Container(
            color: greyColor,
            child: Column(
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.all(10.0),
                  margin: EdgeInsets.all(10.0),
                  color: whiteColor,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(10.0),
                        child: new ClipRRect(
                            borderRadius: new BorderRadius.circular(10.0),
                            child: new Container(
                                height: 50.0,
                                width: 50.0,
                                child: new Image.asset('assets/image/taxi-driver.png',fit: BoxFit.cover, height: 100.0,width: 100.0,)
                            )
                        ),
                      ),
                      Container(
                        width: screenSize.width - 100,
                        padding: EdgeInsets.only(left: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    child: Text("Steve Bowen",style: textBoldBlack,),
                                  ),
                                  Container(
                                      child: Text("\$25.0",style: heading18Black,)
                                  ),

                                ],
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("VISA",style: textBoldBlack,),
                                Text("2.2km",style: textGrey,)
                              ],
                            ),
                          ],
                        )
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  color: whiteColor,
                  child: HistoryTrip(
                    fromAddress: "994 Colin Gateway Suite, 981",
                    toAddress: "8753 Mauricio Walks",
                  ),
                ),
                new Container(
                  margin: EdgeInsets.all(10.0),
                  padding: EdgeInsets.all(10.0),
                  color: whiteColor,
                  child: Column(
                    children: <Widget>[
                      new Row(
                        children: <Widget>[
                          new Text("Bill Details (Cash Payment)", style: textBoldBlack,),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text("Ride Fare", style: textStyle,),
                            new Text("\$10.99", style: textBoldBlack,),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text("Taxes", style: textStyle,),
                            new Text("\$1.99", style: textBoldBlack,),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8.0,bottom: 8.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text("Discount", style: textStyle,),
                            new Text("- \$5.99", style: textBoldBlack,),
                          ],
                        ),
                      ),
                      Container(
                        width: screenSize.width - 50.0,
                        height: 1.0,
                        color: Colors.grey.withOpacity(0.4),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8.0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            new Text("Total Bill", style: heading18Black,),
                            new Text("\$7.49", style: heading18Black,),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Form(
                  key: formKey,
                  child: Container(
                    margin: EdgeInsets.all(10.0),
                    padding: EdgeInsets.all(10.0),
                    color: whiteColor,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        FlutterRatingBar(
                          initialRating: 4,
                          fillColor: Colors.amber,
                          borderColor: Colors.amber.withAlpha(50),
                          allowHalfRating: true,
                          itemSize: 30.0,
                          onRatingUpdate: (rating) {
                            ratingScore = rating;
                            print(rating);
                          },
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 10.0),
                          child: new SizedBox(
                            height: 100.0,
                            child: new TextField(
                              style: new TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                              ),
                              decoration: InputDecoration(
                                hintText: "Write your review",
                                hintStyle: TextStyle(
                                  color: Colors.black38,
                                  fontFamily: 'Akrobat-Bold',
                                  fontSize: 16.0,
                                ),
                                border: OutlineInputBorder(
                                    borderRadius:BorderRadius.circular(5.0)),
                              ),
                              maxLines: 2,
                              keyboardType: TextInputType.multiline,
                              onChanged: (String value) { setState(() => yourReview = value );},
                            ),
                          ),
                        ),
                        ButtonTheme(
                          minWidth: screenSize.width,
                          height: 45.0,
                          child: RaisedButton(
                            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                            elevation: 0.0,
                            color: primaryColor,
                            child: new Text('Submit',style: headingWhite,
                            ),
                            onPressed: (){
                              Navigator.of(context).pushReplacementNamed('/history');
                              //and
                              Navigator.popAndPushNamed(context, '/history');
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
