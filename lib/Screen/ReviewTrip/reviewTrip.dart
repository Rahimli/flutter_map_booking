import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:flutter_map_booking/Components/historyTrip.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ReviewTripScreens extends StatefulWidget {
  @override
  _ReviewTripScreensState createState() => _ReviewTripScreensState();
}

class _ReviewTripScreensState extends State<ReviewTripScreens> {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  String yourReview;
  double ratingScore;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    submit(){
      formKey.currentState.save();
      Navigator.pushNamed(context, '/home');
    }

    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: blackColor),
        elevation: 2.0,
//        centerTitle: true,
        backgroundColor: whiteColor,
        title: Text('Review your trip',style: TextStyle(color: blackColor),),
      ),
      body: SingleChildScrollView(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
          child: Container(
            color: greyColor,
            padding: EdgeInsets.only(bottom: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  height: 170.0,
                  width: double.infinity,
                  color: primaryColor,
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Text('\$12',style: heading35,),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Image.asset("assets/image/car1.png",height: 70.0,)
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 10.0),
                        child: Text('November 11, 2019 AT 4.00 PM',style: heading18,),
                      )
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10.0),
                  color: whiteColor,
                  padding: EdgeInsets.all(10.0),
                  child: HistoryTrip(
                    fromAddress: "994 Colin Gateway Suite, 981",
                    toAddress: "8753 Mauricio Walks",
                  ),
                ),
                Container(
                  color: greyColor,
                  padding: EdgeInsets.all(10.0),
                  child: Material(
                      borderRadius: BorderRadius.circular(0.0),
                      child: Container(
                          padding: EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Form(
                            key: formKey,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  child: Material(
                                    elevation: 5.0,
                                    borderRadius: BorderRadius.circular(100.0),
                                    child: new ClipRRect(
                                        borderRadius: new BorderRadius.circular(100.0),
                                        child: new Container(
                                            height: 100.0,
                                            width: 100.0,
//                      color: Color(getColorHexFromStr('#FDD148')),
                                            child: new Image.asset('assets/image/taxi-driver.png',fit: BoxFit.cover, height: 100.0,width: 100.0,)
                                        )
                                    ),
                                  ),
                                ),
                                FlutterRatingBar(
                                  initialRating: 4,
                                  fillColor: Colors.amber,
                                  borderColor: Colors.amber.withAlpha(50),
                                  allowHalfRating: true,
                                  itemSize: 30.0,
                                  onRatingUpdate: (rating) {
                                    ratingScore = rating;
                                    print(rating);
                                  },
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 10.0),
                                  child: new SizedBox(
                                    height: 100.0,
                                    child: new TextField(
                                      style: new TextStyle(
                                        color: Colors.black,
                                        fontSize: 18.0,
                                      ),
                                      decoration: InputDecoration(
                                        hintText: "Write your review",
                                        hintStyle: TextStyle(
                                          color: Colors.black38,
                                          fontFamily: 'Akrobat-Bold',
                                          fontSize: 16.0,
                                        ),
                                        border: OutlineInputBorder(
                                            borderRadius:BorderRadius.circular(5.0)),
                                      ),
                                      maxLines: 2,
                                      keyboardType: TextInputType.multiline,
                                      onChanged: (String value) { setState(() => yourReview = value );},
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                      )
                  ),
                ),
                Container(
                  color: greyColor,
                  margin: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ButtonTheme(
                        minWidth: screenSize.width*0.43,
                        height: 45.0,
                        child: OutlineButton(
                            color: blackColor,
                            textColor: blackColor,
                            child: Text('Skip'),
                            onPressed:(){print('skip');Navigator.pushNamed(context, '/home');}
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 30.0),),
                      ButtonTheme(
                        minWidth: screenSize.width*0.43,
                        height: 45.0,
                        child: RaisedButton(
                          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                          elevation: 0.0,
                          color: primaryColor,
                          child: new Text('Submit',style: headingWhite,
                          ),
                          onPressed: (){
                            submit();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )

      ),
    );
  }
}
