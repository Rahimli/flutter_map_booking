import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:flutter_map_booking/Blocs/place_bloc.dart';
import 'package:flutter_map_booking/Model/placeItem.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:flutter/cupertino.dart';

class SearchAddress extends StatefulWidget {
  final String selectedAddress;
  final Function(PlaceItemRes, bool) onSelected;
  final bool _isFromAddress;
  SearchAddress(this.selectedAddress, this.onSelected, this._isFromAddress);

  @override
  _SearchAddressState createState() => _SearchAddressState();
}

class _SearchAddressState extends State<SearchAddress> {
  var _addressController;
  var placeBloc = PlaceBloc();

  @override
  void initState() {
    _addressController = TextEditingController(text: widget.selectedAddress);
    super.initState();
  }

  @override
  void dispose() {
    placeBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: whiteColor,
        elevation: 2.0,
        title: Text("Search address",
          style: TextStyle(color: blackColor),
        ),
        iconTheme: IconThemeData(
          color: blackColor
        ),
      ),
      body: new SingleChildScrollView(
//        padding: EdgeInsets.all(10.0),
//        constraints: BoxConstraints.expand(),
//        color: Color(0xfff8f8f8),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: CupertinoTextField(
                  prefix: Icon(
                    CupertinoIcons.location_solid,
                    color: greyColor2,
                    size: 28.0,
                  ),
                  padding: EdgeInsets.symmetric(horizontal: 6.0, vertical: 12.0),
                  clearButtonMode: OverlayVisibilityMode.editing,
                  keyboardType: TextInputType.text,
                  autocorrect: true,
                  autofocus: true,
                  decoration: BoxDecoration(
                    color: CupertinoTheme.of(context).scaffoldBackgroundColor,
                    //border: Border(bottom: BorderSide(width: 0.0, color: CupertinoColors.inactiveGray)),
                  ),
                  placeholder: 'Select address',
                  controller: _addressController,
                  textInputAction: TextInputAction.search,
                  onChanged: (str) {
                    placeBloc.searchPlace(str);
                  },
                ),
              ),
            ),
            Container(
              height: 300.0,
              padding: EdgeInsets.only(top: 20),
              child: StreamBuilder(
                  stream: placeBloc.placeStream,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data == "start") {
                        return Center(
                          child: CupertinoActivityIndicator(),
                        );
                      }
                      List<PlaceItemRes> places = snapshot.data;
                      return ListView.separated(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(places.elementAt(index).name),
                              subtitle: Text(places.elementAt(index).address),
                              onTap: () {
                                FocusScope.of(context).requestFocus(new FocusNode());
                                Navigator.of(context).pop();
                                widget.onSelected(places.elementAt(index),
                                    widget._isFromAddress);
                              },
                            );
                          },
                          separatorBuilder: (context, index) => Divider(
                            height: 1,
                            color: Color(0xfff5f5f5),
                          ),
                          itemCount: places.length);
                    } else {
                      return Container();
                    }
                  }),
            )
          ],
        ),
      ),
    );
  }
}
