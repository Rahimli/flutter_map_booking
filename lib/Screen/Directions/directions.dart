import 'package:flutter/material.dart';
import 'package:flutter_map_booking/Components/inputField.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_map_booking/Apis.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_map_booking/config.dart';
import 'package:flutter_map_booking/Components/decodePolyline.dart';
import 'package:flutter_map_booking/Model/placeItem.dart';
import 'dart:io' show Platform;
import 'package:flutter_map_booking/Screen/Home/home.dart';
import 'selectService.dart';
import 'package:flutter_map_booking/Components/customDialogInput.dart';
import 'package:flutter_map_booking/Components/loading.dart';
import 'dart:async';

class DirectionsScreen extends StatefulWidget {
  final makersDir;
  final place;

  DirectionsScreen({this.makersDir,this.place});

  @override
  _DirectionsScreenState createState() => _DirectionsScreenState();
}

class _DirectionsScreenState extends State<DirectionsScreen> {
  Apis api = Apis();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<LatLng> points = <LatLng>[];
  GoogleMapController _mapController;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId selectedMarker;
  BitmapDescriptor _markerIcon;

  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};
  int _polylineIdCounter = 1;
  PolylineId selectedPolyline;

  bool checkPlatform = Platform.isIOS;
  String distance, duration;
  bool isLoading = false;
  bool isResult = false;

  void _onMapCreated(GoogleMapController controller) {
    this._mapController = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addMakers();
    _createPoints();
//    _moveCamera();
  }

  @override
  void dispose() {
    super.dispose();
  }

//  Future<void> _createMarkerImageFromAsset(BuildContext context) async {
//    if (_markerIcon == null) {
//      final ImageConfiguration imageConfiguration =
//      createLocalImageConfiguration(context);
//      BitmapDescriptor.fromAssetImage(
//          imageConfiguration, 'image/icons8-place-marker-96.png')
//          .then(_updateBitmap);
//    }
//  }
//
//  void _updateBitmap(BitmapDescriptor bitmap) {
//    setState(() {
//      _markerIcon = bitmap;
//    });
//  }


  addMakers(){
    checkPlatform ? print('ios'): print("adnroid");
    final MarkerId markerIdFrom = MarkerId("from_address");
    final MarkerId markerIdTo = MarkerId("to_address");
    var LatLngFrom =  widget.makersDir[markerIdFrom].position;
    var LatLngTo =  widget.makersDir[markerIdTo].position;

    final Marker marker = Marker(
      markerId: markerIdFrom,
      position: LatLng(LatLngFrom.latitude, LatLngFrom.longitude),
      infoWindow: InfoWindow(title: widget.place.name, snippet: widget.place.address),
      icon:  checkPlatform ? BitmapDescriptor.fromAsset("assets/image/gps_point_24.png") : BitmapDescriptor.fromAsset("assets/image/gps_point.png"),
      onTap: () {
        // _onMarkerTapped(markerId);
      },
    );

    final Marker markerTo = Marker(
      markerId: markerIdTo,
      position: LatLng(LatLngTo.latitude, LatLngTo.longitude),
      infoWindow: InfoWindow(title: widget.place.name, snippet: widget.place.address),
      icon: checkPlatform ? BitmapDescriptor.fromAsset("assets/image/ic_marker_32.png") : BitmapDescriptor.fromAsset("assets/image/ic_marker_128.png"),
      onTap: () {
        // _onMarkerTapped(markerId);
      },
    );

    setState(() {
      markers[markerIdFrom] = marker;
      markers[markerIdTo] = markerTo;
    });
  }

  _createPoints() async {
    List<dynamic> _points = <dynamic>[];
    List<dynamic> latLong = <dynamic>[];
    List<dynamic> lngLong = <dynamic>[];

    points.clear();
    final MarkerId markerIdFrom = MarkerId("from_address");
    final MarkerId markerIdTo = MarkerId("to_address");
    var LatLngFrom =  widget.makersDir[markerIdFrom].position;
    var LatLngTo =  widget.makersDir[markerIdTo].position;

    String query = 'origin=${LatLngFrom.latitude},${LatLngFrom
        .longitude}&destination=${LatLngTo.latitude},${LatLngTo
        .longitude}&key=$ApiKey&sensor=false&mode=driving';
    print(query);
    var data = await api.getCallAPI(query);
    var codePoints = decode(data['routes'][0]['overview_polyline']['points']);
    distance = data['routes'][0]['legs'][0]['distance']['text'];
    duration = data['routes'][0]['legs'][0]['duration']['text'];


    for (int lat = 0; lat < codePoints.length; lat += 2) {
      setState(() {
        latLong.add(codePoints[lat]);
      });
    }

    for (int lng = 1; lng < codePoints.length; lng += 2) {
      setState(() {
        lngLong.add(codePoints[lng]);
      });
    }

    for (int i = 0; i < latLong.length; i++) {
      _points.add([latLong[i], lngLong[i]]);
    }

    for (int i = 0; i < _points.length; i++) {
      points.add(_createLatLng(_points[i][0], _points[i][1]));
    }

    final String polylineIdVal = 'polyline_id_$_polylineIdCounter';
    _polylineIdCounter++;
    final PolylineId polylineId = PolylineId(polylineIdVal);

    final Polyline polyline = Polyline(
      polylineId: polylineId,
      consumeTapEvents: true,
      color: Colors.orange,
      width: checkPlatform ? 4 : 8,
      points: points,
      onTap: () {
//        _onPolylineTapped(polylineId);
      },
    );

    setState(() {
      polylines[polylineId] = polyline;
      _moveCamera(LatLngFrom,LatLngTo);
    });
  }

  _moveCamera(var LocationFrom, LocationTo){
    var LatLngFrom = LocationFrom;
    var LatLngTo = LocationTo;
    var sLat, sLng, nLat, nLng;

    if(LatLngFrom.latitude <= LatLngTo.latitude) {
      sLat = LatLngFrom.latitude;
      nLat = LatLngTo.latitude;
    } else {
      sLat = LatLngTo.latitude;
      nLat = LatLngFrom.latitude;
    }

    if(LatLngFrom.longitude <= LatLngTo.longitude) {
      sLng = LatLngFrom.longitude;
      nLng = LatLngTo.longitude;
    } else {
      sLng = LatLngTo.longitude;
      nLng = LatLngFrom.longitude;
    }

    _mapController.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(sLat, sLng),
          northeast: LatLng(nLat, nLng),
        ),
        15.0,
      ),
    );
  }

  LatLng _createLatLng(double lat, double lng) {
    return LatLng(lat, lng);
  }
  dialogOption(){
    return customDialogInput(
      title: "Option",
      body: "",
      buttonName: "Confirm",
      inputValue: TextFormField(
        style: textStyle,
        keyboardType: TextInputType.text,
        decoration: new InputDecoration(
          //border: InputBorder.none,
          hintText: "Ex: I'm standing in front of the bus stop...",
          // hideDivider: true
        ),
      ),
      onPressed: (){print(Navigator.of(context).pop());print('Option');},
    );
  }

  dialogPromoCode(){
    return customDialogInput(
      title: "Promo Code",
      body: "",
      buttonName: "Confirm",
      inputValue: TextFormField(
        style: textStyle,
        keyboardType: TextInputType.text,
        decoration: new InputDecoration(
          //border: InputBorder.none,
          hintText: "Enter promo code",
          // hideDivider: true
        ),
      ),
      onPressed: (){print(Navigator.of(context).pop());print('Option');},
    );
  }

  handSubmit(){
    print("Submit");
    setState(() {
      isLoading = true;
    });

     new Timer(new Duration(seconds: 5), ()
     {
       setState(() {
         isLoading = false;
          isResult = true;
       });
     });
  }

  @override
  Widget build(BuildContext context) {
//    _createMarkerImageFromAsset(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(
                      height: MediaQuery.of(context).size.height - 250,
                      child: GoogleMap(
                        onMapCreated: _onMapCreated,
                        //myLocationEnabled: true,
                        initialCameraPosition: CameraPosition(
                          target: LatLng(21.003473, 105.848949),
                          zoom: 13,
                        ),
                        markers: Set<Marker>.of( markers.values),
                        polylines: Set<Polyline>.of(polylines.values),
                      )
                  ),
                  isLoading == true ?
                  Container(
                      height: 250.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            child: LoadingBuilder(),
                          )
                        ],
                      )
                  ):isResult == true ? result(context) :
                  booking(context),
                ],
              ),
              Positioned(
                left: 0,
                top: 0,
                right: 0,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    AppBar(
                      backgroundColor: Colors.transparent,
                      elevation: 0.0,
                      centerTitle: true,
                      leading: FlatButton(
                          onPressed: () {
                            Navigator.of(context).pushReplacement(
                                new MaterialPageRoute(builder: (context) => new HomeScreen()));
                          },
                          child: Icon(FontAwesomeIcons.arrowAltCircleLeft,color: blackColor,)
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget booking(BuildContext context){
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.grey,
            child: Column(
              children: <Widget>[
                Container(
                  color: whiteColor,
                  height: 50.0,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Container(
                            padding: EdgeInsets.only(left: 10.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(right: 10.0),
                                  child: Icon(FontAwesomeIcons.car),
                                ),
                                Text('GrabTaxi',style: textStyle,),
                              ],
                            )
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Container(
                          child: Text(distance != null ? distance : "0 km",style: textGrey,),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Text(duration != null ? duration : "0 mins",style: textGrey,),
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width - 50.0,
                  height: 1.0,
                  color: Colors.grey.withOpacity(0.4),
                ),
                new Container(
                  color: whiteColor,
                  padding: EdgeInsets.only(top: 10.0,bottom: 10.0),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                          flex: 3,
                          child: new GestureDetector(
                             onTap: () {
                               Navigator.of(context).push(new MaterialPageRoute<Null>(
                                   builder: (BuildContext context) {
                                     return SelectService();
                                   },
                                   fullscreenDialog: true));
                             },
                              child:  new Column(
                                children: <Widget>[
                                  new Icon(FontAwesomeIcons.indent,color: greyColor2,),
                                  new Text("Service",style: textGrey,),
                                ],
                              )
                          )
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                        width: 1.0,
                        height: 30.0,
                        color: Colors.grey.withOpacity(0.4),
                      ),
                      new Expanded(
                        flex: 3,
                        child: new GestureDetector(
                           onTap: () =>showDialog(context: context, child: dialogOption()),
                            child: new Column(
                              children: <Widget>[
                                new Icon(FontAwesomeIcons.cogs,color: greyColor2,),
                                new Text("Options",style: textGrey,),
                              ],
                            )
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                        width: 1.0,
                        height: 30.0,
                        color: Colors.grey.withOpacity(0.4),
                      ),
                      new Expanded(
                          flex: 3,
                          child: GestureDetector(
                            onTap: () => showDialog(context: context, child: dialogPromoCode()),
                            child: new Column(
                              children: <Widget>[
                                new Icon(FontAwesomeIcons.gifts,color: greyColor2,),
                                new Text("Promo",style: textGrey,),
                              ],
                            )
                          ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 50.0,
            height: 1.0,
            color: Colors.grey.withOpacity(0.4),
          ),
          Container(
            padding: EdgeInsets.only(top: 10.0,bottom: 10.0,left: 20.0,right: 20.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Container(
                      padding: EdgeInsets.all(3.0),
                      decoration: BoxDecoration(
                        color: primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 10.0),
                            child: Icon(FontAwesomeIcons.wallet,color: greyColor2,),
                          ),
                          Column(
                            children: <Widget>[
                              Text("\$8 - \$10",style: heading18Black,),
                              Text("Cash",style: textGrey,)
                            ],
                          ),
                        ],
                      ),
                    )
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                  width: 1.0,
                  height: 30.0,
                  color: Colors.grey.withOpacity(0.4),
                ),
                Expanded(
                    flex: 2,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 10.0),
                            child: Icon(FontAwesomeIcons.creditCard,color: greyColor2,),
                          ),
                          Column(
                            children: <Widget>[
                              Text("\$8",style: heading18Black,),
                              Text("VISA",style: textGrey,)

                            ],
                          ),
                        ],
                      ),
                    )
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width - 50.0,
            height: 1.0,
            color: Colors.grey.withOpacity(0.4),
          ),
          Container(
            padding: EdgeInsets.only(top: 10.0),
            child: ButtonTheme(
              minWidth: MediaQuery.of(context).size.width - 50.0,
              height: 45.0,
              child: RaisedButton(
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                elevation: 0.0,
                color: primaryColor,
                child: new Text('Book Now',style: headingWhite,
                ),
                onPressed: (){
                  handSubmit();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget result(BuildContext context){
    return Container(
      padding: EdgeInsets.only(left: 15.0,right: 15.0,top: 10.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(100.0),
                  child: new ClipRRect(
                    borderRadius: new BorderRadius.circular(100.0),
                    child: new Container(
                      height: 100.0,
                      width: 100.0,
//                      color: Color(getColorHexFromStr('#FDD148')),
                      child: new Image.asset('assets/image/taxi-driver.png',fit: BoxFit.cover, height: 100.0,width: 100.0,)
                    )
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("John Doe",style: headingBlack,),
                    Text("0135549874",style: textStyle,),
                    Text("Toyota Black - MP09QA1222",style: textStyle,)
                  ],
                ),
              ),
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Column(
                 // crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Icon(Icons.call,color: greyColor2,size: 30.0,),
                    Text("Call Driver",style: textStyle,)
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                width: 1.0,
                height: 30.0,
                color: Colors.grey.withOpacity(0.4),
              ),
              Expanded(
                flex: 5,
                child: Column(
                  children: <Widget>[
                    Text("\$19",style: headingBlack,)
                  ],
                )
              ),
            ],
          ),
          Divider(),
          ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            height: 45.0,
            child: RaisedButton(
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
              elevation: 0.0,
              color: primaryColor,
              child: new Text('Complete',style: headingWhite,
              ),
              onPressed: (){
                Navigator.pushNamed(context, '/review_trip');
              },
            ),
          ),
        ],
      ),
    );
  }
}
