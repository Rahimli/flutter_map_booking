import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_map_booking/Apis.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_map_booking/config.dart';
import 'package:flutter_map_booking/Components/decodePolyline.dart';
import 'dart:io' show Platform;
import 'selectService.dart';
import 'package:flutter_map_booking/Components/customDialogInput.dart';
import 'package:flutter_map_booking/Components/loading.dart';
import 'dart:async';
import 'package:flutter_map_booking/Components/customDialogInfo.dart';

class DirectionsScreen2 extends StatefulWidget {
  List<Map<String, dynamic>> dataFrom;
  List<Map<String, dynamic>> dataTo;

  DirectionsScreen2({this.dataFrom,this.dataTo});

  @override
  _DirectionsScreen2State createState() => _DirectionsScreen2State();
}

class _DirectionsScreen2State extends State<DirectionsScreen2> {
  Apis api = Apis();
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<LatLng> points = <LatLng>[];
  GoogleMapController _mapController;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId selectedMarker;
  BitmapDescriptor _markerIcon;

  Map<PolylineId, Polyline> polylines = <PolylineId, Polyline>{};
  int _polylineIdCounter = 1;
  PolylineId selectedPolyline;

  bool checkPlatform = Platform.isIOS;
  String distance, duration;
  bool isLoading = false;
  bool isResult = false;
  LatLng positionDriver;
  bool isComplete = false;

  void _onMapCreated(GoogleMapController controller) {
    this._mapController = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    addMakers();
    _createPoints();
  }

  @override
  void dispose() {
    super.dispose();
  }

  addMakers(){
    checkPlatform ? print('ios'): print("adnroid");
    final MarkerId markerIdFrom = MarkerId("from_address");
    final MarkerId markerIdTo = MarkerId("to_address");


    var _dataFrom =  widget.dataFrom;
    var _dataTo =  widget.dataTo;

    final Marker marker = Marker(
      markerId: markerIdFrom,
      position: LatLng(_dataFrom[0]['lat'], _dataFrom[0]['long']),
      infoWindow: InfoWindow(title: _dataFrom[0]['name'], snippet: _dataFrom[0]['address']),
      icon:  checkPlatform ? BitmapDescriptor.fromAsset("assets/image/gps_point_24.png") : BitmapDescriptor.fromAsset("assets/image/gps_point.png"),
      onTap: () {
        // _onMarkerTapped(markerId);
      },
    );

    final Marker markerTo = Marker(
      markerId: markerIdTo,
      position: LatLng(_dataTo[0]['lat'], _dataTo[0]['long']),
      infoWindow: InfoWindow(title: _dataTo[0]['name'], snippet: _dataTo[0]['address']),
      icon: checkPlatform ? BitmapDescriptor.fromAsset("assets/image/ic_marker_32.png") : BitmapDescriptor.fromAsset("assets/image/ic_marker_128.png"),
      onTap: () {
        // _onMarkerTapped(markerId);
      },
    );

    setState(() {
      markers[markerIdFrom] = marker;
      markers[markerIdTo] = markerTo;
    });
  }

  ///Calculate and return the best router
  _createPoints() async {
    List<dynamic> _points = <dynamic>[];
    List<dynamic> latLong = <dynamic>[];
    List<dynamic> lngLong = <dynamic>[];

    points.clear();

    var latFrom = widget.dataFrom[0]['lat'];
    var lngFrom = widget.dataFrom[0]['long'];
    var latTo = widget.dataTo[0]['lat'];
    var lngTo = widget.dataTo[0]['long'];

    String query = 'origin=$latFrom,$lngFrom&destination=$latTo,$lngTo&key=$ApiKey&sensor=false&mode=driving';
    print(query);
    var data = await api.getCallAPI(query);
    var codePoints = decode(data['routes'][0]['overview_polyline']['points']);
    distance = data['routes'][0]['legs'][0]['distance']['text'];
    duration = data['routes'][0]['legs'][0]['duration']['text'];
    var listStepLocation = data['routes'][0]['legs'][0]['steps'];

    for (int lat = 0; lat < codePoints.length; lat += 2) {
      setState(() {
        latLong.add(codePoints[lat]);
      });
    }

    for (int lng = 1; lng < codePoints.length; lng += 2) {
      setState(() {
        lngLong.add(codePoints[lng]);
      });
    }

    for (int i = 0; i < latLong.length; i++) {
      _points.add([latLong[i], lngLong[i]]);
    }

    for (int i = 0; i < _points.length; i++) {
      points.add(_createLatLng(_points[i][0], _points[i][1]));
    }

    final String polylineIdVal = 'polyline_id_$_polylineIdCounter';
    _polylineIdCounter++;
    final PolylineId polylineId = PolylineId(polylineIdVal);

    final Polyline polyline = Polyline(
      polylineId: polylineId,
      consumeTapEvents: true,
      color: Colors.orange,
      width: checkPlatform ? 4 : 8,
      points: points,
      onTap: () {
//        _onPolylineTapped(polylineId);
      },
    );

    setState(() {
      polylines[polylineId] = polyline;
      _moveCamera(latFrom,lngFrom,latTo,lngTo);
    });
  }

  ///Real-time test of driver's location
  ///My data is demo.
  ///This function works by: every 5 or 2 seconds will request for api and after the data returns,
  ///the function will update the driver's position on the map.

  runTrackingDriver(var _listPosition){
    int count = 0;
    const timeRequest = const Duration(seconds: 2);
    Timer.periodic(timeRequest, (Timer t) {
      positionDriver = _listPosition[count++];
      print(positionDriver);
      addMakersDriver(positionDriver);
      if(count == _listPosition.length){
        setState(() {
          t.cancel();
          isComplete = true;
          showDialog(context: context, child: dialogInfo());
        });

      }
    });

  }

  addMakersDriver(LatLng _position){
    final MarkerId markerDriver = MarkerId("driver");
    final Marker marker = Marker(
      markerId: markerDriver,
      position: _position,
      icon: checkPlatform ? BitmapDescriptor.fromAsset("assets/image/icon_car_32.png") : BitmapDescriptor.fromAsset("assets/image/icon_car_120.png"),
      draggable: false,
      onTap: () {
        // _onMarkerTapped(markerId);
      },
    );
    setState(() {
      markers[markerDriver] = marker;
    });
  }

  _moveCamera(var latFrom, lngFrom, latTo, lngTo){
    var _latFrom = latFrom;
    var _lngFrom = lngFrom;
    var _latTo = latTo;
    var _lngTo = lngTo;


    var sLat, sLng, nLat, nLng;

    if(_latFrom <= _latTo) {
      sLat = _latFrom;
      nLat = _latTo;
    } else {
      sLat = _latTo;
      nLat = _latFrom;
    }

    if(_lngFrom <= _lngTo) {
      sLng = _lngFrom;
      nLng = _lngTo;
    } else {
      sLng = _lngTo;
      nLng = _lngFrom;
    }

    _mapController.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(sLat, sLng),
          northeast: LatLng(nLat, nLng),
        ),
        15.0,
      ),
    );
  }

  LatLng _createLatLng(double lat, double lng) {
    return LatLng(lat, lng);
  }

  dialogOption(){
    return customDialogInput(
      title: "Option",
      body: "",
      buttonName: "Confirm",
      inputValue: TextFormField(
        style: textStyle,
        keyboardType: TextInputType.text,
        decoration: new InputDecoration(
          //border: InputBorder.none,
          hintText: "Ex: I'm standing in front of the bus stop...",
          // hideDivider: true
        ),
      ),
      onPressed: (){print(Navigator.of(context).pop());print('Option');},
    );
  }

  dialogPromoCode(){
    return customDialogInput(
      title: "Promo Code",
      body: "",
      buttonName: "Confirm",
      inputValue: TextFormField(
        style: textStyle,
        keyboardType: TextInputType.text,
        decoration: new InputDecoration(
          //border: InputBorder.none,
          hintText: "Enter promo code",
          // hideDivider: true
        ),
      ),
      onPressed: (){print(Navigator.of(context).pop());print('Option');},
    );
  }

  handSubmit(){
    print("Submit");
    setState(() {
      isLoading = true;
    });

    new Timer(new Duration(seconds: 5), ()
    {
      setState(() {
        isLoading = false;
        isResult = true;
      });
    });
  }

  dialogInfo(){
    return customDialogInfo(
      title: "Information",
      body: "Trip completed. Review your trip now!.",
      onTap: (){
        Navigator.of(context).pop();
        Navigator.pushNamed(context, '/review_trip');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
//    _createMarkerImageFromAsset(context);

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Stack(
            children: <Widget>[
              new Column(
                children: <Widget>[
                  SizedBox(
                      height: MediaQuery.of(context).size.height - 200,
                      child: GoogleMap(
                        onMapCreated: _onMapCreated,
                        myLocationEnabled: true,
                        myLocationButtonEnabled: false,
                        initialCameraPosition: CameraPosition(
                          target: LatLng(widget.dataFrom[0]['lat'], widget.dataFrom[0]['long']),
                          zoom: 13,
                        ),
                        markers: Set<Marker>.of( markers.values),
                        polylines: Set<Polyline>.of(polylines.values),
                      )
                  ),
                  isLoading == true ?
                  Container(
                      height: 200.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            child: LoadingBuilder(),
                          )
                        ],
                      )
                  ):isResult == true ? result(context) :
                  booking(context),
                ],
              ),
              Positioned(
                left: 0,
                top: 0,
                right: 0,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    AppBar(
                      backgroundColor: Colors.transparent,
                      elevation: 0.0,
                      centerTitle: true,
                      leading: FlatButton(
                          onPressed: () {
                            Navigator.of(context).pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
                          },
                          child: Icon(FontAwesomeIcons.arrowAltCircleLeft,color: blackColor,)
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget booking(BuildContext context){
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.grey,
            child: Column(
              children: <Widget>[
                Container(
                  color: whiteColor,
                  height: 60.0,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Container(
                            padding: EdgeInsets.only(left: 10.0),
                            child: Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(right: 10.0),
                                  child: Icon(FontAwesomeIcons.car),
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text('GrabTaxi',style: textStyle,),
                                      Text('Near by you',style: textGrey,),
                                    ],
                                  ),
                                )

                              ],
                            )
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
                          child: Text(distance != null ? distance : "0 km",style: textGrey,),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
                          margin: EdgeInsets.only(right: 10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("\$20.35",style: headingPrimaryColor,),
                              Text(duration != null ? duration : "0 mins",style: textGrey,),
                            ],
                          ),
                        )
                      )
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: greyColor,
                ),
                new Container(
                  color: whiteColor,
                  padding: EdgeInsets.only(top: 15.0,bottom: 15.0),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                          flex: 3,
                          child: new GestureDetector(
                              onTap: () {
                                Navigator.of(context).push(new MaterialPageRoute<Null>(
                                    builder: (BuildContext context) {
                                      return SelectService();
                                    },
                                    fullscreenDialog: true));
                              },
                              child:  new Column(
                                children: <Widget>[
                                  new Icon(FontAwesomeIcons.indent,color: greyColor2,),
                                  new Text("Service",style: textGrey,),
                                ],
                              )
                          )
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                        width: 1.0,
                        height: 30.0,
                        color: Colors.grey.withOpacity(0.4),
                      ),
                      new Expanded(
                        flex: 3,
                        child: new GestureDetector(
                            onTap: () =>showDialog(context: context, child: dialogOption()),
                            child: new Column(
                              children: <Widget>[
                                new Icon(FontAwesomeIcons.cogs,color: greyColor2,),
                                new Text("Options",style: textGrey,),
                              ],
                            )
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                        width: 1.0,
                        height: 30.0,
                        color: Colors.grey.withOpacity(0.4),
                      ),
                      new Expanded(
                        flex: 3,
                        child: GestureDetector(
                            onTap: () => showDialog(context: context, child: dialogPromoCode()),
                            child: new Column(
                              children: <Widget>[
                                new Icon(FontAwesomeIcons.gifts,color: greyColor2,),
                                new Text("Promo",style: textGrey,),
                              ],
                            )
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          Container(
            padding: EdgeInsets.only(top: 10.0,bottom: 10.0),
            child: ButtonTheme(
              minWidth: MediaQuery.of(context).size.width - 50.0,
              height: 45.0,
              child: RaisedButton(
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                elevation: 0.0,
                color: primaryColor,
                child: new Text('Book Now',style: headingWhite,
                ),
                onPressed: (){
                  handSubmit();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget result(BuildContext context){

    return Container(
      padding: EdgeInsets.only(left: 8.0,right: 8.0,top: 10.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                  height: 50.0,
                  width: 50.0,
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(100.0),
                    child: new ClipRRect(
                        borderRadius: new BorderRadius.circular(100.0),
                        child: new Container(
                            height: 50.0,
                            width: 50.0,
                            child: new Image.asset('assets/image/taxi-driver.png',fit: BoxFit.cover, height: 100.0,width: 100.0,)
                        )
                    ),
                  ),
                ),
              Expanded(
                flex: 5,
                child: Container(
                  padding: EdgeInsets.only(left: 20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("John Doe",style: headingBlack,),
                      Text("Toyota Black ",style: textGrey,),
                      Text("MP09QA1222",style: textGrey,)
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 3,
                child: Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(100.0),),
                        ),
                        child: IconButton(
                          icon: Icon(FontAwesomeIcons.facebookMessenger,color: whiteColor,size: 25.0,),
                        ),
                      ),
                      Padding(padding: EdgeInsets.only(left: 5.0),),
                      Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.all(Radius.circular(100.0),),
                        ),
                        child: IconButton(
                          icon: Icon(Icons.phone,color: whiteColor,size: 25.0,),
                        ),
                      ),
                    ],
                  ),
                )
              ),
            ],
          ),
          Divider(),
          Row(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Distance",style: textGrey,),
                    Text("4.3 km",style: textStyle,),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                width: 1.0,
                height: 30.0,
                color: Colors.grey.withOpacity(0.4),
              ),
              Expanded(
                flex: 3,
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Time",style: textGrey,),
                    Text("15 min",style: textStyle,),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 20.0,bottom: 20.0),
                width: 1.0,
                height: 30.0,
                color: Colors.grey.withOpacity(0.4),
              ),
              Expanded(
                  flex: 5,
                  child: Column(
                    children: <Widget>[
                      Text("\$19.23",style: headingBlack,)
                    ],
                  )
              ),
            ],
          ),
          Divider(),
          isComplete == false ?
            ButtonTheme(
              minWidth: MediaQuery.of(context).size.width,
              height: 45.0,
              child: RaisedButton(
                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                elevation: 0.0,
                color: primaryColor,
                child: new Text('Confirm',style: headingWhite,
                ),
                onPressed: (){
                  runTrackingDriver(points);
                },
              ),
            ): ButtonTheme(
            minWidth: MediaQuery.of(context).size.width,
            height: 45.0,
            child: RaisedButton(
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
              elevation: 0.0,
              color: primaryColor,
              child: new Text('Review',style: headingWhite,
              ),
              onPressed: (){
                Navigator.pushNamed(context, '/review_trip');
              },
            ),
          ),
        ],
      ),
    );
  }
}
