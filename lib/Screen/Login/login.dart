import 'package:flutter/material.dart';
import 'package:flutter_map_booking/Components/inputField.dart';
import 'package:flutter_map_booking/Components/validations.dart';
import 'package:flutter_map_booking/theme/style.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool autovalidate = false;
  Validations validations = new Validations();

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      key: scaffoldKey,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              colorFilter: new ColorFilter.mode(Colors.black.withOpacity(0.6), BlendMode.darken),
              image: ExactAssetImage('assets/image/login.jpg'),fit: BoxFit.cover)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Flexible(
                flex: 4,
                child: new Center(
                  child: new Image(
                    image: ExactAssetImage('assets/image/pizza_icon.png'),
                    height: screenSize.height / 3.5,
                  ),
                ),
              ),
              new Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Container(
                      padding: EdgeInsets.only(bottom: 20.0),
                      margin: new EdgeInsets.symmetric(
                          horizontal: 20.0),
                      child: new Form(
                        key: formKey,
                        autovalidate: autovalidate,
                        child: new Column(
                          children: <Widget>[
                            new Column(
                              children: <Widget>[
                                new InputField(
                                  hintText: "Email",
                                  obscureText: false,
                                  textInputType: TextInputType.text,
                                  textStyle: textStyleWhite,
                                  textFieldColor: textFieldColor,
                                  icon: Icons.email,
                                  iconColor: Colors.white,
                                  bottomMargin: 25.0,
                                  hintStyle: TextStyle(color: Colors.white),
                                  validateFunction: validations.validateEmail,
                                  onSaved: (String value) {},
                                ),
                                new InputField(
                                  hintText: "Password",
                                  obscureText: true,
                                  textInputType:
                                  TextInputType.text,
                                  textStyle: textStyleWhite,
                                  textFieldColor:
                                  textFieldColor,
                                  icon: Icons.lock_open,
                                  iconColor: Colors.white,
                                  bottomMargin: 25.0,
                                  hintStyle: TextStyle(color: Colors.white),
                                  validateFunction: validations
                                      .validatePassword,
                                  onSaved: (String value){},
                                )],
                            ),
                            ButtonTheme(
                              minWidth: screenSize.width,
                              height: 60.0,
                              child: RaisedButton(
                                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                                elevation: 0.0,
                                color: primaryColor,
                                child: new Text('LOGIN',style: headingWhite,),
                                onPressed: (){
                                  Navigator.pushNamed(context, '/walkthrough2');
                                },
                              ),
                            ),
                            new Container(
                              padding: new EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 30.0),
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 2,
                                      child: new GestureDetector(
                                      onTap: () => Navigator.pushNamed(context, '/signup'),
                                      child: new Text("Create new account?",
                                        style: textStyleWhite,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  new Expanded(
                                    flex: 2,
                                    child: new GestureDetector(
                                      onTap: () => Navigator.pushNamed(context, '/forgot_password'),
                                      child: new Text("Forgot Password?",
                                        style: textStyleWhite,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ),
                            new Container(
                              padding: new EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
                              child: new Row(
                                children: <Widget>[
                                  new Expanded(
                                    flex: 2,
                                    child: ButtonTheme(
                                      minWidth: 200.0,
                                      height: 40.0,
                                      child: RaisedButton.icon(
                                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                                        elevation: 0.0,
                                        color: googlePlus,
                                        icon: Icon(FontAwesomeIcons.googlePlus,color: whiteColor,),
                                        label: new Text('Google +',style: headingWhite18,),
                                        onPressed: (){
                                          Navigator.pushNamed(context, '/home');
                                        },
                                      ),
                                    ),
                                  ),
                                  new SizedBox(
                                    width: 10.0,
                                  ),
                                  new Expanded(
                                    flex: 2,
                                    child: ButtonTheme(
                                      minWidth: 200.0,
                                      height: 40.0,
                                      child: RaisedButton.icon(
                                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                                        elevation: 0.0,
                                        color: facebook,
                                        icon: Icon(FontAwesomeIcons.facebook,color: whiteColor,),
                                        label: new Text('Facebook',style: headingWhite18,
                                        ),
                                        onPressed: (){
                                          Navigator.pushNamed(context, '/home');
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
